import React from "react";
import AppLayout from "../../components/AppLayout";
import HeadLayout from "../../components/HeadLayout";
import {Button} from "@material-ui/core";
import {useRouter} from "next/router";
import userSign from "../../util/userSign";
import {dehydrate, QueryClient, useMutation, useQuery} from "react-query";

import {confirmAlert} from "../../util/sweetalert2";
import Moment from "react-moment";
import nl2br from "react-nl2br";
import {baseUrl} from "../../hooks/api";
import {deleteGalleryRemove, getGalleryView} from "../../hooks/api/galleryApi";
import {getBoardView} from "../../hooks/api/boardApi";
import BoardComment from "../../components/BoardComment";

const View = () => {
    const [user,setUser] = userSign();
    const logout = () => {
        setUser(null);
    }
    const Router = useRouter();
    const params = Router.query;//url 파라미터 받기
    const id = params.id;//아이디 값만 받기
    //게시판 상세보기
    const {isLoading,status,data,error} = useQuery(
        ['gallery_view', {id:id}],
        getGalleryView,
        {enabled:true}); //enabled는 서버에 자동 요청할 것인지 여부
    //게시판 삭제 통신하기
    const boardRemove = useMutation(
        'boardRemove',
        deleteGalleryRemove,
        {
            onSuccess:(data,variables,context) => {
                Router.replace("/gallery/list");
            },
            onError:(error,variables,context) =>{

            }
        }
    );
    //목록 버튼 클릭
    const onGoListClick = () => {
        Router.push("/gallery/list");
    }
    //수정 버튼 클릭
    const onUpdateClick = () => {
        Router.replace(`/gallery/form?id=${id}`);
    }
    //삭제 버튼 클릭
    const onRemoveClick = () => {
        confirmAlert('갤러리 삭제','삭제하시겠습니까?')
            .then((result) => {
                if(result.isConfirmed){
                    boardRemove.mutate({board_user_id:data?.user_id,user_id:user.user_id,id:data?.id});
                }
            });
    }
    if(isLoading){
        return <div>로딩중</div>
    }
    return (
        <AppLayout user={user} logout={logout}>
            <HeadLayout title="게시판 상세보기"></HeadLayout>
            <div className="board-view">
                <div className="board-view-subject">{data?.subject}</div>
                <div className="board-view-info">
                    <div className="board-view-writer">{data?.user_name}</div>
                    <div className="board-view-date"><Moment format="YY/MM/DD">{data?.createdAt}</Moment></div>
                </div>
                <div className="board-view-content">
                    <img src={`${baseUrl}/uploads/${data?.image}`} width="90%"/><br/>
                    {nl2br(data.content)}
                </div>
                <div className="board-view-btn">
                    {user&&user?.user_id===data?.user_id?<Button color="primary" variant="contained" onClick={onUpdateClick}>수정</Button>:null}&nbsp;
                    {user&&user?.user_id===data?.user_id?<Button color="secondary" variant="contained" onClick={onRemoveClick}>삭제</Button>:null}&nbsp;
                    <Button color="default" variant="contained" onClick={onGoListClick}>목록</Button>
                </div>
                <div className="comment">
                    <BoardComment  user={user} board='gallery' id={id}/>
                </div>
            </div>
        </AppLayout>
    )
}

export default View;