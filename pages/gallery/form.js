import React, {useRef, useState,useCallback,useEffect} from "react";
import AppLayout from "../../components/AppLayout";
import HeadLayout from "../../components/HeadLayout";
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import LoginIcon from "@mui/icons-material/Login";
import {LoadingButton} from "@mui/lab";

import {useRouter} from "next/router";
import Button from "@mui/material/Button";
import AddPhotoAlternateIcon from "@mui/icons-material/AddPhotoAlternate";
import api, {baseUrl} from "../../hooks/api";
import userSign from "../../util/userSign";
import {useMutation, useQuery} from "react-query";

import {getGalleryView} from "../../hooks/api/galleryApi";
import useInput from "../../hooks/useInput";
import {errorAlert} from "../../util/sweetalert2";

const Form = () => {
    const Router = useRouter();
    const params = Router.query;//url 파라미터 받기
    const id = params.id;//아이디 값만 받기
    const [user,setUser] = userSign();//회원정보

    const logout = () => {
        setUser(null);
        Router.push("/gallery/list");
    }
    //갤러리 데이터 가져오기
    const {data} = useQuery(
        ['board_view', {id:id}],
        getGalleryView,
        {enabled:true}); //enabled는 서버에 자동 요청할 것인지 여부

    const [user_name,onChangeUserName,setUserName] = useInput('');
    const [subject,onChangeSubject,setSubject] = useInput('');
    const [content,onChangeContent,setContent] = useInput('');

    const [user_id,setUserId] = useState('');
    const [mode,setMode] = useState('');
    const [loading,setLoading] = useState(false);
    const [galleryImage,setGalleryImage] = useState('');
    //useRef 설정
    const userNameRef = useRef(null);
    const subjectRef = useRef(null);
    const contentRef = useRef(null);
    const imageRef=useRef();
    //회원 로그인 할 때 변수 재설정하기
    useEffect(()=>{
        if(user){
            setUserId(user.user_id);
            setUserName(user.user_name);
        }
        if(data){
            setSubject(data.subject);
            setContent(data.content);
            setGalleryImage(data.image);
            setMode('update')
        }
    },[user,data]);

    //서버 통신 리액트쿼리
    //회원가입 서버 통신 하기
    const galleryWrite = useMutation(
        'userRegister',
        async params => {
            await api.post(`/gallery/write`,params)
                .then((result)=>{
                    if(mode==='update'){
                        Router.replace(`/gallery/view?id=${id}`);
                    }else {
                        Router.replace("/gallery/list");
                    }
                })
                .catch((error)=>{
                    console.log(error);
                })
        },
    );

    const onImageChange = useCallback(async (e) => {
        const formData = new FormData();
        formData.append('image', e.target.files[0]);
        const image=await api.post("/gallery/upload",formData,{
            headers: {
                "Content-Type": "multipart/form-data"
            }
        });
        setGalleryImage(image.data);
        console.log(image);
    },[]);


    //글쓰기 버튼을 눌렀을 때
    const onWriteClick = useCallback(() => {
        if(subject.length < 1){
            errorAlert(subjectRef,'제목 입력 오류','제목을 입력하세요');
            return;
        }
        if(content.length < 1){
            errorAlert(contentRef,'내용 입력 오류','내용을 입력하세요');
            return;
        }
        const gallery = {
            user_id,
            user_name,
            subject,
            content,
            image:galleryImage,
            mode:mode,
        }
        if(mode==='update'){
            console.log(mode);
            gallery.id=id;
        }
        console.log(gallery);
        galleryWrite.mutate(gallery);
        setLoading(true);
    },[user_id,user_name,subject,content,galleryImage]);
    if(user===''){
        Router.push("/board/list");
        return (
            <div>
                회원 로그인 후 글쓰기를 할 수 있습니다.
            </div>
        )
    }
    return (
        <AppLayout user={user} logout={logout}>
            <HeadLayout title="게시판 글쓰기"></HeadLayout>
            <Box
                component="form"
                noValidate
                autoComplete="off"
            >
                <h2>게시판 글쓰기</h2>
                <div>
                    <TextField
                        fullWidth
                        id="outlined-search"
                        label="이름"
                        value={user_name}
                        inputRef={userNameRef}
                        onChange={onChangeUserName}
                        type="text" />
                </div>
                <br/>
                <div>
                    <TextField
                        fullWidth
                        id="outlined-search"
                        label="제목"
                        type="search"
                        value={subject}
                        inputRef={subjectRef}
                        onChange={onChangeSubject}
                    />
                </div>
                <br/>
                <div>
                    <TextField
                        fullWidth
                        id="outlined-search"
                        label="내용"
                        type="search"
                        rows={4}
                        value={content}
                        inputRef={contentRef}
                        onChange={onChangeContent}
                        multiline/>
                </div>
                <div>
                    <input
                        accept="image/*"
                        style={{display:'none'}}
                        type="file"
                        id="images"
                        ref={imageRef}
                        onChange={onImageChange}
                    />
                    <label htmlFor="images">
                        <Button
                            startIcon={<AddPhotoAlternateIcon/>}
                            variant="contained"
                            onClick={()=>{
                                imageRef.current.click();
                            }}
                        >사진 업로드</Button>
                    </label>
                    {
                        galleryImage?
                            <div>
                                <img src={`${baseUrl}/uploads/${galleryImage}`} width="100"/>
                            </div>:null
                    }
                </div>
                <LoadingButton
                    className="login-btn"
                    variant="outlined"
                    loading={loading}
                    loadingPosition="start"
                    onClick={onWriteClick}
                    startIcon={<LoginIcon/>}
                >
                    {
                        !loading?`${mode==='update'?'수정':'등록'}하기`:`${mode==='update'?'수정':'등록'}중...`
                    }

                </LoadingButton>
            </Box>
        </AppLayout>
    )
}

export default Form;