import React,{useEffect} from "react";
import HeadLayout from "../../components/HeadLayout";
import Grid from "@mui/material/Unstable_Grid2";
import {Button, Card, CardContent, CardHeader, CardMedia} from "@material-ui/core";
import AppLayout from "../../components/AppLayout";
import CreateOutlinedIcon from "@mui/icons-material/CreateOutlined";
import {useRouter} from "next/router";
import userSign from "../../util/userSign";
import {useInView} from "react-intersection-observer";
import {galleryInfiniteScrollQuery} from "../../hooks/api/galleryApi";
import Moment from "react-moment";
import {baseUrl} from "../../hooks/api";
import Link from "next/link";

const List = () => {
    const [user,setUser] = userSign();
    const logout = () => {
        setUser(null);
    }
    const Router = useRouter();
    const { getGallery, getNextPage, getGalleryIsSuccess, getNextPageIsPossible } = galleryInfiniteScrollQuery();//무한스크롤로 데이터 가져오기

    console.log(getGallery);
    const [ref,isView] =useInView();//데이터가 있는지 없는 확인 하는 것

    useEffect(() => {
        // 맨 마지막 요소를 보고있고 더이상 페이지가 존재하면
        // 다음 페이지 데이터를 가져옴
        if (isView && getNextPageIsPossible) {
            console.log("nextPage");
            getNextPage();
        }
    }, [isView, getGallery]);



    const onGalleryAddClick= () => {
        Router.push("/gallery/form");
    }

    return (
        <div>
            <HeadLayout title="갤러리 게시판"></HeadLayout>
            <AppLayout user={user} logout={logout}>
                <h2>갤러리</h2>
                <div  style={{textAlign:'right'}}>
                    <Button variant="contained" startIcon={<CreateOutlinedIcon/>} onClick={onGalleryAddClick} className="btn-blue">글쓰기</Button>
                </div>
                <Grid container spacing={2}>
                    {
                        // 데이터를 불러오는데 성공하고 데이터가 0개가 아닐 때 렌더링
                        getGalleryIsSuccess && getGallery.pages
                            ? getGallery.pages.map((page_data, page_num) => {
                                const board_page = page_data.row;
                                return board_page.map((data, idx) => {
                                    if (
                                        // 마지막 요소에 ref 달아주기
                                        getGallery.pages.length - 1 === page_num &&
                                        board_page.length - 1 === idx
                                    ) {
                                        return (
                                            // 마지막 요소에 ref 넣기 위해 div로 감싸기
                                            <Grid xs={6} md={4} ref={ref} key={data.id}>
                                                <Link href={`/gallery/view?id=${data.id}`}>
                                                <Card sx={{maxWidth:300}}>.
                                                    <CardHeader
                                                        title={data.subject}
                                                        subheader={<div>{data.user_name}<Moment format="YY/MM/DD">{data.createdAt}</Moment></div>}
                                                    />
                                                    {
                                                        data.image?
                                                            <CardMedia
                                                                component="img"
                                                                height="200"
                                                                image={`${baseUrl}/uploads/${data.image}`}/>:
                                                            <CardMedia
                                                                component="img"
                                                                height="200"
                                                                image="/img/noimage.png"
                                                            />

                                                    }
                                                    <CardContent>

                                                    </CardContent>
                                                </Card>
                                                </Link>
                                            </Grid>
                                        );
                                    } else {
                                        return (
                                            <Grid xs={6} md={4} key={data.id}>
                                                <Link href={`/gallery/view?id=${data.id}`}>
                                                <Card sx={{maxWidth:300}}>.
                                                    <CardHeader
                                                        title={data.subject}
                                                        subheader={<div>{data.user_name}<Moment format="YY/MM/DD">{data.createdAt}</Moment></div>}
                                                    />
                                                    {
                                                        data.image?
                                                            <CardMedia
                                                                component="img"
                                                                height="200"
                                                                image={`${baseUrl}/uploads/${data.image}`}/>:
                                                            <CardMedia
                                                                component="img"
                                                                height="200"
                                                                image="/img/noimage.png"
                                                            />

                                                    }

                                                    <CardContent></CardContent>
                                                </Card>
                                                </Link>
                                            </Grid>
                                        );
                                    }
                                });
                            })
                            : null
                    }
                </Grid>
            </AppLayout>
        </div>
    );
}

export default List;