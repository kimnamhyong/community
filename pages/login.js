import React, {useRef, useState,useCallback,useEffect} from "react";
import {Box, Container, TextField} from "@material-ui/core";
import Head from "next/head";
import {LoadingButton} from "@mui/lab";
import Link from "next/link";
import {useRouter} from "next/router";
import HeadLayout from "../components/HeadLayout";
import LoginIcon from '@mui/icons-material/Login';
import useInput from "../hooks/useInput";
import {errorAlert, errorAlertUrl, successAlertUrl} from "../util/sweetalert2";
import {useMutation} from "react-query";
import api from "../hooks/api";
import userSign from "../util/userSign";

const Login = () => {
    const Router = useRouter();
    const [users,setUser] = userSign();
    //회원 로그인을 하면은 들어오지 못하게
    const [loading,setLoading] = useState(false);
    const [user_id,onChangeUserId] = useInput('');
    const [user_password,onChangeUserPassword] = useInput('');
    const userIdRef =useRef();
    const userPasswordRef=useRef();
    useEffect(() => {
        if(users){
            errorAlertUrl(null,"로그인오류","이미 로그인하였습니다.","/");
        }
    },[users]);

    //회원가입 서버 통신 하기
    const userLogin = useMutation(
        'userLogin',
        async params => {
            await api.post(`/user/login`,params)
                .then((result)=>{
                   localStorage.setItem("user",JSON.stringify(result.data));
                   Router.push("/");
                })
                .catch((error)=>{
                    console.log(error);
                    errorAlert(null,"로그인 실패",error.response.data)
                        .then((res) => {
                            setLoading(false);
                        });
                })
        },
    );
    //로그인 클릭을 할 때
    const onLoginClick = useCallback(()=>{
        if(user_id.length<1){
            errorAlert(null,"아이디 입력 오류","아이디를 입력하세요");
            return;
        }
        if(user_password.length<1){
            errorAlert(null,"비밀번호 입력 오류","비밀번호를 입력하세요");
            return;
        }
        const user = {
            user_id,
            user_password,
            type:'local'
        }
        userLogin.mutate(user);

    },[user_id,user_password]);
    //엔터를 눌렀을 때 로그인 이벤트
    const onLoginKeyup = useCallback((e) => {
        const key = e.key;
        if(key==="Enter"){
            onLoginClick();
        }
    },[user_id,user_password])


    /*() => {
        setLoading(true);
        setTimeout(()=>{
            Router.push('/');
        },1000);
    }*/

    return (
        users?
            <div>
                로그인이 되어있습니다.
            </div>
        :
        <div>
            <HeadLayout title='로그인'/>
            <div style={{backgroundColor:'#ccc',height:'100%',width:'100%'}}>
                <Box
                    component="form"
                    sx={{
                        '& .MuiTextField-root': { m: 1, width: '25ch' },
                    }}
                    noValidate
                    autoComplete="off"
                    className="loginForm"
                >
                    <div style={{backgroundColor:'#20c997',marginTop:'-20px',height:'50px',lineHeight:'50px'}}>
                        <h2 style={{textAlign:'center'}}>IT FOR ONE</h2>
                        <TextField
                            fullWidth
                            label="아이디"
                            id="user-id"
                            variant="outlined"
                            style={{width:'90%'}}
                            onChange={onChangeUserId}
                            onKeyUp={onLoginKeyup}
                            InputProps={{
                                readOnly: loading,
                            }}
                        />
                        <TextField
                            fullWidth label="비밀번호"
                            id="user-password"
                            type="password"
                            variant="outlined"
                            onChange={onChangeUserPassword}
                            onKeyUp={onLoginKeyup}
                            InputProps={{
                                readOnly: loading,
                            }}
                            style={{width:'90%',marginTop:'10px'}}
                        />
                        <LoadingButton
                            className="login-btn"
                            variant="outlined"
                            loading={loading}
                            loadingPosition="start"
                            onClick={onLoginClick}
                            startIcon={<LoginIcon/>}
                        >
                            {
                                !loading?'로그인':'로그인중...'
                            }

                        </LoadingButton>
                        <div>
                            <Link href="#">아이디/비번찾기</Link>
                        </div>
                    </div>
                </Box>
            </div>
        </div>
    );
}

export default Login;