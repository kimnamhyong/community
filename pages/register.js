import React, {useCallback, useRef, useState,useEffect} from "react";
import HeadLayout from "../components/HeadLayout";
import {Box,  FormControl, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";
import Button from '@mui/material/Button';
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import {LoadingButton} from "@mui/lab";
import PersonAddAltOutlinedIcon from '@mui/icons-material/PersonAddAltOutlined';
import {useMutation, useQuery, useQueryClient} from "react-query";
import {getUserIdCheck, postUserPhoto, postUserRegister} from "../hooks/api/userApi";
import useInput from "../hooks/useInput";
import {errorAlert, successAlert, successAlertUrl} from "../util/sweetalert2";
import {autoHypen} from "../util";
import api, {baseUrl} from "../hooks/api";
import userSign from "../util/userSign";
import {useRouter} from "next/router";
const Register = () => {
    const Router = useRouter();
    const [user,setUser] = userSign();//회원정보 가져오기
    console.log(`user:${user}`);
    const [loading,setLoading] = useState(false);//회원가입 버튼을 눌렀을 때 서버 응답할 때까지 로딩하기
    const [userProfile,setUserProfile] = useState('');//유저 프로필 사진
    const [mode,setMode] = useState('');
    const [user_id,onChangeUserId,setUserId] = useInput('');//회원아이디 입력시 user_id에 값을 받을 수 있게
    const [userIdCheckBool,setUserIdCheckBool] = useState(false);
    const [user_password,onChangeUserPassword] = useInput('');
    const [user_password2,onChangeUserPassword2] = useInput('');
    const [user_name,onChangeUserName,setUserName] = useInput('');
    const [user_hp,onChangeUserHp,setUserHp] = useInput('');
    const [user_email,onChangeUserEmail,setUserEmail] = useInput('');
    //ref 설정
    const userIdRef = useRef();
    const userNameRef = useRef();
    const userPasswordRef = useRef();
    const userPassword2Ref = useRef();
    const userHpRef = useRef();
    const userEmailRef = useRef();
    const userProfileRef = useRef();

    //회원 로그인 할 때 변수 재설정하기
    useEffect(()=>{
        if(user){
            setUserId(user.user_id);
            setUserName(user.user_name);
            setUserHp(user.user_hp);
            setUserEmail(user.user_email);
            setUserProfile(user.user_profile);
            setMode('update');

        }
    },[user]);


    //아이디 중복 체크하기 서버 통신하기
    const row = useQuery(
        ['id_check', {user_id:user_id}],
        getUserIdCheck,
        {enabled:false}); //enabled는 서버에 자동 요청할 것인지 여부
    //회원가입 서버 통신 하기
    const userRegister = useMutation(
        'userRegister',
        async params => {
            await api.post(`/user/register`,params)
                .then((result)=>{
                    if(mode==='update'){
                        localStorage.setItem("user",JSON.stringify(result.data));
                        Router.push("/");
                    }else {
                        successAlertUrl("회원가입 성공", "회원가입하였습니다.", "/login");
                    }
                })
                .catch((error)=>{
                    console.log(error);
                    errorAlert(null,"회원가입 실패",error.response.data.error)
                        .then((res) => {
                            setLoading(false);
                        });
                })
        },
    );
    useEffect(()=>{
        console.log(`data : ${userRegister.data}`);
    },[userRegister]);

    try {

        //아이디 중복체크 결과
        if(row.data.is_check ===false){
            setUserIdCheckBool(false);
            errorAlert(userIdRef,"아이디중복 결과","아이디가 중복되었습니다");
            row.remove();
        }else if(row.data.is_check){
            setUserIdCheckBool(true);
            successAlert("아이디중복체크 결과",`${user_id} 이 아이디는 중복되지 않았습니다.`);
            row.remove();//이거를 해야 usecallback에서 호출하지 않음
        }
    }catch (err){

    }

    //이벤트 모음
    //전화번호 자동 하이픈 이벤트
    const onAutoHypenInput = (e) => {
        setLoading(false);
        autoHypen(e,setUserHp);
    }
    //이메일에서 tab 키를 눌렀을 때 @와 .이 표시 되게
    const onAutoEmail = useCallback((e) => {
        console.log(e.key);
        // if(e.key==='Shift'){
        //     if(e.target.value.indexOf("@") < 0){
        //         e.target.value=e.target.value+'@';
        //         setUserEmail(e.target.value);
        //         return;
        //     }
        // }
    },[user_email]) ;

    //아이디 중복 체크 서버에 요청하기
    const onBlurIdCheck = useCallback((e) =>{
        e.preventDefault();
        setLoading(false);
        if(!user) {
            if (0 < user_id.length) {
                row.refetch();
            } else {
                errorAlert(userIdRef, "아이디 중복 체크 오류", "아이디를 입력하십시오");
            }
        }
    },[user_id]);
    //프로필 사진 첨부시
    const onProfileUploadChange = useCallback(async (e) => {
        const formData = new FormData();
        formData.append('image', e.target.files[0]);
        const image=await api.post("/user/upload",formData,{
            headers: {
                "Content-Type": "multipart/form-data"
            }
        });
        setUserProfile(image.data);
        console.log(image);
    },[]);
    //회원가입 눌렀을 시
    const onRegisterClick = useCallback((e) => {
        row.remove();
        if(user_id.length < 1){
            errorAlert(userIdRef,"아이디 입력 오류","아이디를 입력하세요");
            return;
        }
        if(mode==='') {

            if (userIdCheckBool !== true) {
                errorAlert(userIdRef, "아이디 중복체크 오류", "아이디를 중복체크하십시오");
                return;
            }
            if (user_password.length < 1) {
                errorAlert(userPasswordRef, "비밀번호 입력 오류", "비밀번호를 입력하세요");
                return;
            }
        }
        if(user_password !== user_password2){
            errorAlert(userPassword2Ref,"비밀번호 입력 오류","비밀번호가 맞지 않습니다");
            return;
        }


        if(user_name.length < 1){
            errorAlert(userNameRef,"이름 입력 오류","이름을 입력하십시오");
            return;
        }
        if(user_email.length !== 0 ) {
            if (user_email.indexOf("@") < 1) {
                errorAlert(userEmailRef, "이메일 형식 오류", "이메일에 @를 입력하셔야 합니다.");
                return;
            }
            if (user_email.indexOf(".") < 1) {
                errorAlert(userEmailRef, "이메일 형식 오류", "이메일에 도메인이 입력되지 않았습니다");
                return;
            }
        }
        const user = {
            user_id,
            user_password,
            user_password2,
            user_name,
            user_email,
            user_profile:userProfile,
            user_hp,
            mode
        }
        try{
            userRegister.mutate(user);
        }catch(error){
        }
        setLoading(true);
    },[user_id,user_password,user_password2,user_name,user_hp,user_email,userProfile,userIdCheckBool,row]);

    return (
        <div>
            <HeadLayout title='회원가입'/>
            <Box
                component="form"
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="off"
                className="registerForm"
            >
                <h2>회원가입</h2>
                <TextField
                    fullWidth
                    label="아이디"
                    id="user-id"
                    variant="outlined"
                    onBlur={onBlurIdCheck}
                    onChange={onChangeUserId}
                    inputRef={userIdRef}
                    value={user_id}
                    inputProps={
                        {readOnly:user?true:false}
                    }
                    style={{width:'90%'}}
                />
                <TextField
                    fullWidth label="비밀번호"
                    id="user-password"
                    type="password"
                    variant="outlined"
                    inputRef={userPasswordRef}
                    onChange={onChangeUserPassword}
                    style={{width:'90%',marginTop:'10px'}}
                />
                <TextField
                    fullWidth label="비밀번호 재학인"
                    id="user-password"
                    type="password"
                    variant="outlined"
                    inputRef={userPassword2Ref}
                    onChange={onChangeUserPassword2}
                    style={{width:'90%',marginTop:'10px'}}
                />
                <TextField
                    fullWidth
                    label="이름"
                    id="user-name"
                    variant="outlined"
                    onChange={onChangeUserName}
                    inputRef={userNameRef}
                    value={user_name}
                    style={{width:'90%',marginTop:'10px'}}
                />
                <TextField
                    fullWidth label="휴대폰번호"
                    id="start-date"
                    type="tel"
                    variant="outlined"
                    onKeyUp={onAutoHypenInput}
                    onChange={onChangeUserHp}
                    value={user_hp}
                    style={{width:'90%',marginTop:'10px'}}
                />
                <TextField
                    fullWidth label="이메일"
                    id="user-email"
                    type="email"
                    variant="outlined"
                    value={user_email}
                    onChange={onChangeUserEmail}
                    onKeyDown={onAutoEmail}
                    inputRef={userEmailRef}
                    style={{width:'90%',marginTop:'10px'}}
                />
                <div style={{padding:'10px',width:'90%',textAlign:'left',margin:'0 auto'}}>
                <input
                    accept="image/*"
                    style={{display:'none'}}
                    type="file"
                    id="images"
                    ref={userProfileRef}
                    onChange={onProfileUploadChange}
                />
                <label htmlFor="images">
                    <Button
                        startIcon={<AddPhotoAlternateIcon/>}
                        variant="contained"
                        onClick={()=>{
                            userProfileRef.current.click();
                        }}
                    >프로필 업로드</Button>
                </label>
                {
                    userProfile?
                        <div>
                            <img src={`${baseUrl}/uploads/${userProfile}`} width="100"/>
                        </div>:null
                }
                </div>
                <LoadingButton
                    className="login-btn"
                    variant="outlined"
                    loading={loading}
                    loadingPosition="start"
                    onClick={onRegisterClick}
                    startIcon={<PersonAddAltOutlinedIcon/>}
                    style={{marginTop:'20px',height:'40px'}}
                    >
                    {
                        !loading?`회원${user?'수정':'가입'}`:`회원${user?'수정':'가입'}중...`
                    }
                </LoadingButton>

            </Box>
        </div>
    );
}

export default Register;