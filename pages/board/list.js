import React, {useEffect, useState} from "react";
import HeadLayout from "../../components/HeadLayout";
import {Button} from "@material-ui/core";
import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';
import {useRouter} from "next/router";
import AppLayout from "../../components/AppLayout";
import Link from "next/link";
import {boardInfiniteScrollQuery, getBoardList} from "../../hooks/api/boardApi";
import nl2br from "react-nl2br";
import Moment  from 'react-moment';
import { useInView } from 'react-intersection-observer';
import userSign from "../../util/userSign";

const List = () => {
    const [user,setUser] = userSign();
    const logout = () => {
        setUser(null);
    }
    const Router = useRouter();
    const onBoardAddClick = () => {
        Router.push("/board/form");
    }
    const { getBoard, getNextPage, getBoardIsSuccess, getNextPageIsPossible } = boardInfiniteScrollQuery();//무한스크롤로 데이터 가져오기
    const [ref,isView] =useInView();//데이터가 있는지 없는 확인 하는 것

    useEffect(() => {
        // 맨 마지막 요소를 보고있고 더이상 페이지가 존재하면
        // 다음 페이지 데이터를 가져옴
        if (isView && getNextPageIsPossible) {
            getNextPage();
        }
    }, [isView, getBoard]);




    return (
        <div>
            <HeadLayout title="게시판"></HeadLayout>
            <AppLayout user={user} logout={logout}>
                <div>
                    <div style={{textAlign: 'right'}}>
                        <Button variant="contained" startIcon={<CreateOutlinedIcon/>} onClick={onBoardAddClick}
                                className="btn-blue">글쓰기</Button>
                    </div>
                    {
                        // 데이터를 불러오는데 성공하고 데이터가 0개가 아닐 때 렌더링
                        getBoardIsSuccess && getBoard.pages
                            ? getBoard.pages.map((page_data, page_num) => {
                                const board_page = page_data.row;
                                return board_page.map((data, idx) => {
                                    if (
                                        // 마지막 요소에 ref 달아주기
                                        getBoard.pages.length - 1 === page_num &&
                                        board_page.length - 1 === idx
                                    ) {
                                        return (
                                            // 마지막 요소에 ref 넣기 위해 div로 감싸기
                                            <div ref={ref} key={data.id}>
                                                <Link href={`/board/view?id=${data.id}`}>
                                                    <div className="board-list">
                                                        <div className="board-list-subject">
                                                            {data.subject}
                                                        </div>
                                                        <div className="board-list-content">
                                                            {nl2br(data.content)}
                                                        </div>
                                                        <div className="board-list-writer">{data.user_name}</div>
                                                        <div className="board-list-date">
                                                            <Moment format="YY/MM/DD">{data.createdAt}</Moment>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                        );
                                    } else {
                                        return (
                                            <Link href={`/board/view?id=${data.id}`} key={data.id}>
                                                <div className="board-list">
                                                    <div className="board-list-subject">
                                                        {data.subject}
                                                    </div>
                                                    <div className="board-list-content">
                                                        {nl2br(data.content)}
                                                    </div>
                                                    <div className="board-list-writer">{data.user_name}</div>
                                                    <div className="board-list-date">
                                                        <Moment format="YY/MM/DD">{data.createdAt}</Moment>
                                                    </div>
                                                </div>
                                            </Link>
                                        );
                                    }
                                });
                            })
                            : null
                    }

                </div>
            </AppLayout>
        </div>
    );
}

export default List;