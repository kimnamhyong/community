import React, {useCallback, useRef, useState,useEffect} from "react";
import AppLayout from "../../components/AppLayout";
import HeadLayout from "../../components/HeadLayout";
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import LoginIcon from "@mui/icons-material/Login";
import {LoadingButton} from "@mui/lab";

import {useRouter} from "next/router";
import userSign from "../../util/userSign";
import useInput from "../../hooks/useInput";
import {useMutation, useQuery} from "react-query";
import api from "../../hooks/api";
import {errorAlert} from "../../util/sweetalert2";
import {getBoardView} from "../../hooks/api/boardApi";


const Form = () => {
    const Router = useRouter();
    const params = Router.query;//url 파라미터 받기
    const id = params.id;//아이디 값만 받기
    const [user,setUser] = userSign();//회원정보
    const logout = () => {
        setUser(null);
        Router.push("/board/list");
    }
    //게시판 데이터 가져오기
    const {data} = useQuery(
        ['board_view', {id:id}],
        getBoardView,
        {enabled:true}); //enabled는 서버에 자동 요청할 것인지 여부
    //use state use callback 설정
    const [user_name,onChangeUserName,setUserName] = useInput('');
    const [subject,onChangeSubject,setSubject] = useInput('');
    const [content,onChangeContent,setContent] = useInput('');

    const [user_id,setUserId] = useState('');
    const [mode,setMode] = useState('');
    const [loading,setLoading] = useState(false);

    //useRef 설정
    const userNameRef = useRef(null);
    const subjectRef = useRef(null);
    const contentRef = useRef(null);

    console.log(`user:${user}`);


    //회원 로그인 할 때 변수 재설정하기
    useEffect(()=>{

        if(user){
            setUserId(user.user_id);
            setUserName(user.user_name);
        }else{

        }
        if(data){
            setSubject(data.subject);
            setContent(data.content);
            setMode('update')
        }
    },[user,data]);

    //서버 통신 리액트쿼리
    //회원가입 서버 통신 하기
    const boardWrite = useMutation(
        'boardUpdate',
        async params => {
            await api.post(`/board/write`,params)
                .then((result)=>{
                    if(mode==='update'){
                        Router.replace(`/board/view?id=${id}`);
                    }else {
                        Router.replace("/board/list");
                    }
                })
                .catch((error)=>{
                    console.log(error);
                })
        },
    );



    //글쓰기 버튼을 눌렀을 때
    const onWriteClick = useCallback(() => {
        if(subject.length < 1){
            errorAlert(subjectRef,'제목 입력 오류','제목을 입력하세요');
            return;
        }
        if(content.length < 1){
            errorAlert(contentRef,'내용 입력 오류','내용을 입력하세요');
            return;
        }
        const board = {
            user_id,
            user_name,
            subject,
            content,
            mode:mode,
        }
        if(mode==='update'){
            console.log(mode);
            board.id=id;
        }
        console.log(board);
        boardWrite.mutate(board);
        setLoading(true);
    },[user_id,user_name,subject,content]);
    if(user===''){
        Router.push("/board/list");
        return (
            <div>
                회원 로그인 후 글쓰기를 할 수 있습니다.
            </div>
        )
    }
    return (
        <AppLayout user={user} logout={logout}>
            <HeadLayout title="게시판 글쓰기"></HeadLayout>
            <Box
                component="form"
                noValidate
                autoComplete="off"
            >
                <h2>게시판 글쓰기</h2>
                <div>
                    <TextField
                        fullWidth
                        id="outlined-search"
                        label="이름"
                        value={user_name}
                        inputRef={userNameRef}
                        onChange={onChangeUserName}
                        type="text" />
                </div>
                <br/>
                <div>
                    <TextField
                        fullWidth
                        id="outlined-search"
                        label="제목"
                        type="search"
                        value={subject}
                        inputRef={subjectRef}
                        onChange={onChangeSubject}
                    />
                </div>
                <br/>
                <div>
                    <TextField
                        fullWidth
                        id="outlined-search"
                        label="내용"
                        type="search"
                        rows={4}
                        value={content}
                        inputRef={contentRef}
                        onChange={onChangeContent}
                        multiline/>
                </div>
                <LoadingButton
                    className="login-btn"
                    variant="outlined"
                    loading={loading}
                    loadingPosition="start"
                    onClick={onWriteClick}
                    startIcon={<LoginIcon/>}
                >
                    {
                        !loading?`${mode==='update'?'수정':'등록'}하기`:`${mode==='update'?'수정':'등록'}중...`
                    }

                </LoadingButton>
            </Box>
        </AppLayout>
    )
}

export default Form;