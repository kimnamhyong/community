import React from "react";
import AppLayout from "../../components/AppLayout";
import HeadLayout from "../../components/HeadLayout";
import {Button} from "@material-ui/core";
import {useRouter} from "next/router";
import {dehydrate, QueryClient, useMutation, useQuery} from "react-query";
import Moment  from 'react-moment';
import nl2br from "react-nl2br";
import {deleteBoardRemove, getBoardView} from "../../hooks/api/boardApi";
import userSign from "../../util/userSign";
import {confirmAlert} from "../../util/sweetalert2";
import BoardComment from "../../components/BoardComment";
const View = () => {
    const [user,setUser] = userSign();
    const logout = () => {
        setUser(null);
    }
    const Router = useRouter();
    const params = Router.query;//url 파라미터 받기
    const id = params.id;//아이디 값만 받기
    //게시판 삭제 통신하기
    const boardRemove = useMutation(
        'boardRemove',
        deleteBoardRemove,
        {
            onSuccess:(data,variables,context) => {
                Router.replace("/board/list");
            },
            onError:(error,variables,context) =>{

            }
        }
    );
    //게시판 상세보기
    const {isLoading,status,data,error} = useQuery(
        ['board_view', {id:id}],
        getBoardView,
        {enabled:true}); //enabled는 서버에 자동 요청할 것인지 여부
    console.log(`data:${data}`);
    if(isLoading){
        return <div>로딩중</div>
    }
    if(error){
        return <div>오류발생</div>
    }


    //목록 버튼 클릭
    const onGoListClick = () => {
        Router.push("/board/list");
    }
    //수정 버튼 클릭
    const onUpdateClick = () => {
        Router.replace(`/board/form?id=${id}`);
    }
    //삭제 버튼 클릭
    const onRemoveClick = () => {
        confirmAlert('게시물 삭제','삭제하시겠습니까?')
            .then((result) => {
                if(result.isConfirmed){
                    boardRemove.mutate({board_user_id:data.user_id,user_id:user.user_id,id:data.id});
                }
            });
    }
    

    return (
        <AppLayout user={user} logout={logout}>
            <HeadLayout title="게시판 상세보기"></HeadLayout>
            <div className="board-view">
                <div className="board-view-subject">{data?.subject!==null?data?.subject:''}</div>
                <div className="board-view-info">
                    <div className="board-view-writer">{data?.user_name}</div>
                    <div className="board-view-date"><Moment format="YY/MM/DD">{data?.createdAt}</Moment></div>
                </div>
                <div className="board-view-content">
                    {nl2br(data?.content)}
                </div>
                <div className="board-view-btn">
                    {user&&user.user_id===data?.user_id?<Button color="primary" variant="contained" onClick={onUpdateClick}>수정</Button>:null}&nbsp;
                    {user&&user.user_id===data?.user_id?<Button color="secondary" variant="contained" onClick={onRemoveClick}>삭제</Button>:null}&nbsp;
                    <Button color="default" variant="contained" onClick={onGoListClick}>목록</Button>
                </div>
                <div className="comment">
                    <BoardComment  user={user} board='board' id={id}/>
                </div>
            </div>


        </AppLayout>
    )
}


export default View;