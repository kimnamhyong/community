import React from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import '/public/css/main.css';
import { Hydrate, QueryClient, QueryClientProvider } from "react-query";//리액트 쿼리를 쓰려면 이게 필요함
import { ReactQueryDevtools } from "react-query/devtools";

const App = ({Component,pageProps}) => {
    const [queryClient] = React.useState(() => new QueryClient({
        defaultOptions:{
            queries: {
                retry: false,
            },
        }
    }));
    return (
        <>
            <Head>
                <title>앱</title>
            </Head>
            {/* 리액트 쿼리 세팅하기 */}
            <QueryClientProvider client={queryClient}>
                <Hydrate state={pageProps.dehydratedState}>
                    <Component />
                </Hydrate>
                <ReactQueryDevtools/>
            </QueryClientProvider>
        </>
    );
}

App.propTypes = {
    Component: PropTypes.elementType.isRequired
}

export default App;