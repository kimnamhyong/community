import React, {useEffect, useState} from "react";
import AppLayout from "../components/AppLayout";
import userSign from "../util/userSign";


const Home = () => {
    const [user,setUser] = userSign();
    const logout = () => {
        setUser(null);
    }
    return (
        <div>
            <AppLayout user={user} logout={logout}>
                안녕하세요 안녕하세요. 안녕하세요. 안녕하세요.
            </AppLayout>
        </div>
    );
}


export default Home;