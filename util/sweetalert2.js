import React,{useRef} from "react";
import Swal from "sweetalert2";
import Router,{useRouter} from "next/router";

export const errorAlert = (ref,title,text) => {
    return Swal.fire({
        icon:'error',
        title:title,
        text:text,
        showCloseButton:true,
        focusConfirm:true,
        didClose: () => {
            if(ref!=null) {
                ref.current.focus();
            }
        }
    });
}
export const errorAlertUrl = (ref,title,text,url) => {
    return Swal.fire({
        icon:'error',
        title:title,
        text:text,
        showCloseButton:true,
        focusConfirm:true,
        didClose: () => {
            window.location.replace(url);
        }
    });
}
export const successAlertUrl = (title,text,url) => {
    return Swal.fire({
        icon:'success',
        title:title,
        text:text,
        showCloseButton:true,
        focusConfirm:true,
        didClose: () =>{
            if(url!==""){
                Router.push(url);
            }
        }
    });
}
export const successAlert = (title,text) => {
    return Swal.fire({
        icon: 'success',
        title: title,
        text: text,
        showCloseButton: true,
        focusConfirm: true,
    });
}
export const confirmAlert = (title,text) => {
    return Swal.fire({
        title:title,
        text:text,
        icon:"question",
        confirmButtonText:'확인',
        showCloseButton:true,
        showDenyButton: true,
    });
}


