import React,{useEffect,useState} from "react";

export default () => {
    const [user,set] = useState(null);
     useEffect(()=>{
        const userItem = typeof window !== 'undefined' ? localStorage.getItem("user"):null;
        const users = userItem !== null? JSON.parse(userItem):"";
        set(users);

    },[]);
    return [user,set];
}

/*const userInfo = useEffect(()=>{
    const userItem = typeof window !== 'undefined' ? localStorage.getItem("user"):null;
    const users = userItem !== null? JSON.parse(userItem):"";
    console.log(`users:${users.id}`);
    setUser(users);
},[]);*/