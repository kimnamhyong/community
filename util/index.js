import React,{useRef} from "react";

export const autoHypen = (e,setter) => {
    if(e.key!=='Backspace'||e.key!=='Delete'){
        if(e.target.value.length===3) {
            e.target.value = e.target.value + '-';
        }
        if(e.target.value.length===8){
            e.target.value = e.target.value + '-';
        }
        if(13 < e.target.value.length){
            e.target.value = e.target.value.substring(0,13);
        }
        setter(e.target.value);
    }
}

