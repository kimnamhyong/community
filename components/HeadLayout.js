import React from "react";
import Head from "next/head";
import PropTypes from 'prop-types';


const HeadLayout = ({title}) => {
    return (
        <div>
            <Head>
                <title>{title}</title>
                {/* Tell the browser to never restore the scroll position on load */}
                <script
                    dangerouslySetInnerHTML={{
                        __html: `history.scrollRestoration = "manual"`,
                    }}
                />
            </Head>
        </div>
    )
}

HeadLayout.propTypes = {
    title:PropTypes.string.isRequired
}

export default HeadLayout;