import React, {useCallback, useRef, useState,useEffect} from "react";
import propTypes from 'prop-types';
import {Box, TextField} from "@material-ui/core";
import Button from '@mui/material/Button';
import {LoadingButton} from "@mui/lab";
import useInput from "../hooks/useInput";
import MapsUgcIcon from '@mui/icons-material/MapsUgc';
import {confirmAlert, errorAlert} from "../util/sweetalert2";
import {dehydrate, QueryClient, useMutation, useQuery} from "react-query";
import api, {baseUrl} from "../hooks/api";
import {getCommentList,deleteCommentRemove} from "../hooks/api/commentApi";
import Moment from "react-moment";
import nl2br from "react-nl2br";
import Pagination from "react-js-pagination";
import {deleteBoardRemove} from "../hooks/api/boardApi";

const BoardComment = ({user, board, id}) => {
    const [loading, setLoading] = useState(false);
    const commentRef = useRef();
    const [content, onChangeContent, setContent] = useInput('');
    const [comment_id,setCommentId] = useState(0);
    const [mode,setMode] = useState('');
    const [page,setPage] = useState(1);
    const commentRemove = useMutation(
        'commentRemove',
        deleteCommentRemove,
        {
            onSuccess:(data,variables,context) => {
                row.refetch();
            },
            onError:(error,variables,context) =>{

            }
        }
    );
    //코멘트 목록 가져오기
    const row = useQuery(
        ["commentList", {page: page, type: board, board_id: id}],
        getCommentList,
        {enabled: true}
    );
    //코멘트 서버 전송하기
    const commentWrite = useMutation(
        'commentUpdate',
        async params => {
            await api.post(`/comment/${board}/update`, params)
                .then((result) => {
                    setContent('');
                    setLoading(false);
                    row.refetch();
                })
                .catch((error) => {
                    console.log(error);
                    setLoading(false);
                })
        },
    );
    //댓글 등록을 눌렀을 때
    const onCommentWriteClick = useCallback(() => {
        if (content.length < 2) {
            errorAlert(commentRef, '댓글 내용 오류', '댓글을 입력하세요');
            return;
        }
        const comment = {
            user_id: user.user_id,
            user_name: user.user_name,
            content: content,
            board_id: id,
            page:page,
            mode:mode,
            comment_id:comment_id
        }
        commentWrite.mutate(comment);
        setLoading(true);
        setMode('');
    }, [user,page, content,comment_id,mode]);
    //수정버튼을 눌렀을 때
    const onCommentEditClick = useCallback((data) => {
        setMode('update');
        setCommentId(data.id);
        setContent(data.content);
        commentRef.current.focus();
    },[commentRef]);
    //삭제 버튼을 눌렀을 때
    const onCommentRemoveClick = useCallback((data) => {
        if(mode==='update'){
            errorAlert(null,'댓글 수정중','댓글 수정중에는 삭제를 할 수 없습니다.');
            return;
        }
        confirmAlert('댓글 삭제','삭제하시겠습니까?')
            .then((result) => {
                if(result.isConfirmed){
                    commentRemove.mutate({comment_user_id:data.user_id,user_id:user.user_id,comment_id:data.id,type:board});
                }
            });
    },[board,user,mode]);
    //페이지 눌렀을 때
    const handlePageChange = useCallback((page) => {
        setPage(page);
        row.refetch();
    },[page]);
    //이거를 항상 넣어야 함 안 넣으면 새로 고침시 오류가 발생함
    if(row.isLoading){
        return "loading";
    }
    return (
        <div>
            <div>
                <Box
                    component="form"
                    noValidate
                    autoComplete="off"
                >
                    {
                        user ?
                            <div>

                                <textarea id="content"
                                          className="comment-form"
                                          placeholder="내용을 입력하세요"
                                          ref={commentRef}
                                          value={content}
                                          onChange={onChangeContent}
                                ></textarea>

                                <LoadingButton
                                    className="login-btn"
                                    variant="outlined"
                                    loading={loading}
                                    loadingPosition="start"
                                    onClick={onCommentWriteClick}
                                    startIcon={<MapsUgcIcon/>}

                                >
                                    코멘트 {mode==='update'?'수정':'등록'}
                                </LoadingButton>
                            </div> :
                            <div>댓글은 로그인 후에 쓸 수 있습니다.</div>
                    }
                    {/* 댓글 목록 시작 */}
                    {
                        row.data?.row.map((data, key) => {
                            return (
                                <div className="comment-list">
                                    <div className="comment-title">
                                        <div className="comment-user-profile">
                                            <img src={`${baseUrl}/uploads/${data.profile}`}/>
                                        </div>
                                        <div className="comment-user">
                                            <div className="comment-user-name">{data.user_name}</div>
                                            <Moment format="YY/MM/DD" className="comment-creatAt">{data.createdAt}</Moment>
                                        </div>
                                    </div>
                                    <div className="comment-content">
                                        {nl2br(data.content)}
                                    </div>
                                    {
                                        data.user_id===user?.user_id?
                                            <div className="comment-btn">
                                                <Button
                                                    variant="contained"
                                                    color="secondary"
                                                    onClick={()=>{onCommentEditClick(data)}}
                                                >수정</Button>&nbsp;
                                                <Button
                                                    variant="contained"
                                                    color="error"
                                                    onClick={()=>{onCommentRemoveClick(data)}}
                                                >삭제</Button>
                                            </div>:null
                                    }
                                </div>
                            )
                        })
                    }

                    {/* 댓글 목록 끝 */}
                    <div className="paging">
                    <Pagination
                        activePage={page}
                        itemsCountPerPage={row.data?.pages.limit}
                        totalItemsCount={row.data?.pages.totalCount}
                        pageRangeDisplayed={row.data?.pages.pageSize}
                        prevPageText={"‹"}
                        nextPageText={"›"}
                        onChange={handlePageChange}
                    />
                    </div>

                </Box>
            </div>
        </div>
    );
}

BoardComment.propTypes = {
    user: propTypes.object.isRequired,
    board: propTypes.string.isRequired
}

export default BoardComment;