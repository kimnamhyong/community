import React, {useState, useCallback, useEffect} from "react";
import PropTypes from 'prop-types';
import {AppBar, Container, ListItemButton} from "@mui/material";
import {Divider, Drawer, IconButton, List, ListItemIcon, ListItemText, Toolbar, Typography} from "@material-ui/core";
import PersonIcon from '@mui/icons-material/Person';
import {Menu} from "@material-ui/icons";
import Link from "next/link";

import {baseUrl} from "../hooks/api";

const AppLayout = (props) => {

    const [open, setOpen] = useState(false);
    const drawerWidth = 250;
    //드로우 메뉴 오픈
    const onMenuClick = () => {
        setOpen(!open)
    }
    //드로우 메뉴 닫기
    const onMenuClick2 = () =>{
        setOpen(false);
    }
    const onLogoutClick = useCallback(()=>{
        localStorage.removeItem("user");
        props.logout();

    },[props]);
    return (
        <div>
            {/* 상단 앱바*/}
            <AppBar style={{backgroundColor:'#000',}}>
                <Toolbar>
                    {/* 메뉴바*/}
                    <IconButton edge="start" aria-label="Menu" onClick={onMenuClick}>
                        <Menu style={{color: '#fff'}}/>
                    </IconButton>
                    <Typography variant="h6">
                        <Link href="/"><div style={{color:'#fff'}}>아이티포원</div></Link>
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer variant="persistent" open={open}>
                <div style={{width:`${drawerWidth}px`,height:'181px'}}>
                    <div className="drawer-close" onClick={onMenuClick}>X</div>
                    <div className="left-profile">
                        <ul>
                            <li>
                                <div className="profile-photo">
                                    {
                                        props.user?
                                            <img src={`${baseUrl}/uploads/${props.user.user_profile}`} style={{width:'120px'}}/>:
                                            <PersonIcon style={{fontSize:'120px',color:'gray',marginLeft:'-8px'}}/>
                                    }

                                </div>
                            </li>
                            <li>
                                {
                                    props.user?props.user.user_name:'로그인'
                                }
                            </li>
                            <li>
                                {
                                    props.user?
                                        <div>
                                            <a href="#" onClick={onLogoutClick}>로그아웃</a>
                                            <Link href="/register">회원수정</Link>
                                        </div>:
                                        <div>
                                            <Link href="/login">
                                                로그인
                                            </Link>
                                            <Link href="/register">
                                                회원가입
                                            </Link>
                                        </div>
                                }

                                {/*
                                <Link href="/login">
                                    로그인
                                </Link>
                                <Link href="/register">
                                    회원가입
                                </Link>*/}
                            </li>
                        </ul>
                    </div>
                    <div className="left-menu">
                        <ul>
                            <li>
                                <Link href="/board/list">게시판</Link>
                            </li>
                            <li>
                                <Link href="/gallery/list">갤러리게시판</Link>
                            </li>
                        </ul>
                    </div>
                </div>
                <Divider />
            </Drawer>
            {
                open?
                    <div className="drawer-menu"
                        onClick={onMenuClick2}
                    ></div>:null
            }
            <div style={{
                margin: '70px 0',
                width:'100%'
            }}
            >
                {props.children}
            </div>
        </div>
    );
}

AppLayout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default AppLayout;

