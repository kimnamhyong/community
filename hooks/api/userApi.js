import api from "../api";
//아이디 중복 체크하기
const getUserIdCheck = async ({queryKey}) => {
    const [_key,{user_id}] = queryKey;
    const { data } = await api.get(`/user/id_check?user_id=${user_id}`);
    return data;
}
//회원가입 또는 회원수정 프로필 사진을 첨부를 할 때 서버 연결
const postUserPhoto = async (formData) => {

    const image= await api.post("/user/upload",formData,{
        headers: {
            "Content-Type": "multipart/form-data"
        }
    });
    return image.data;
}
//회원가입을 눌렀을 때
const postUserRegister = async (param) => {
   return await api.post(`/user/register`,param);

}

export {getUserIdCheck,postUserPhoto,postUserRegister};