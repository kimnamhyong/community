import axios from 'axios';
export default axios.create({
    baseURL:"http://172.24.6.131:3061/",
    withCredentials:true,//로그인을 할 때 세션 공유 할 때 필요
    headers: {
        'Content-type': 'application/json; charset=UTF-8',
        accept: 'application/json,',
    }
});

export const baseUrl = 'http://172.24.6.131:3061';