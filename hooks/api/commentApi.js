import api from "../api";

//댓글 목록
const getCommentList = async ({queryKey}) => {
    const [_key,{page,type,board_id}] = queryKey;
    const { data } = await api.get(`/comment/${type}/list?board_id=${board_id}&page=${page}`);

    return data;
}
//게시판 삭제하기
const deleteCommentRemove = async (params) => {
    console.log(params);
    if(params.comment_user_id===params.user_id) {
        const {data} = await api.delete(`/comment/${params.type}/remove/${params.comment_id}`);
        console.log(data);
        return data;
    }else{
        return null;
    }
}
export {getCommentList,deleteCommentRemove}