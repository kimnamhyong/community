import api from "../api";
import { useInfiniteQuery } from "react-query";//무한스크롤 할 때 필요함
//게시판 목록
const getBoardList = async ({queryKey}) => {
    const [_key,{page}] = queryKey;
    const { data } = await api.get(`/board/list?page=${page}`);
    console.log(data);
    return data;
}
//게시판 상세보기
const getBoardView = async ({queryKey}) => {
    const [_key,{id}] = queryKey;
    const { data } = await api.get(`/board/view?id=${id}`);
    console.log(data);
    return data;
}
//게시판 삭제하기
const deleteBoardRemove = async (params) => {
    console.log(params);
    if(params.board_user_id===params.user_id) {
        const {data} = await api.delete(`/board/remove/${params.id}`);
        console.log(data);
        return data;
    }else{
        return null;
    }
}

//무한 스크롤 게시판 목록
export const boardInfiniteScrollQuery = () => {
    const getPageBoard =async ({ pageParam = 1}) => {
        const row = await api.get(`/board/list?page=${pageParam}`);
        return {
            row:row.data,
            current_page:pageParam,
            isLast: row.data.last
        }
    }

    const {
        data: getBoard,
        fetchNextPage: getNextPage,
        isSuccess: getBoardIsSuccess,
        hasNextPage: getNextPageIsPossible
    } = useInfiniteQuery(["page_board_list"], getPageBoard, {
        getNextPageParam: (lastPage, pages) => {
            // lastPage와 pages는 콜백함수에서 리턴한 값을 의미한다!!
            // lastPage: 직전에 반환된 리턴값, pages: 여태 받아온 전체 페이지
            if (!lastPage.isLast) return lastPage.current_page + 1;
            // 마지막 페이지면 undefined가 리턴되어서 hasNextPage는 false가 됨!
            return undefined;
        },
    });
    return { getBoard, getNextPage, getBoardIsSuccess, getNextPageIsPossible };
}

export {getBoardList,getBoardView,deleteBoardRemove}