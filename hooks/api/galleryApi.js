import api from "../api";
import { useInfiniteQuery } from "react-query";//무한스크롤 할 때 필요함
//갤러리 목록
const getGalleryList = async ({queryKey}) => {
    const [_key,{page}] = queryKey;
    const { data } = await api.get(`/gallery/list?page=${page}`);
    console.log(data);
    return data;
}
//갤러리 상세보기
const getGalleryView = async ({queryKey}) => {
    const [_key,{id}] = queryKey;
    const { data } = await api.get(`/gallery/view?id=${id}`);
    console.log(data);
    return data;
}
//갤러리 삭제하기
const deleteGalleryRemove = async (params) => {
    console.log(params);
    if(params.board_user_id===params.user_id) {
        const {data} = await api.delete(`/gallery/remove/${params.id}`);
        console.log(data);
        return data;
    }else{
        console.log("111");
        return null;
    }
}

//무한 스크롤 갤러리 목록
export const galleryInfiniteScrollQuery = () => {
    const getPageGallery =async ({ pageParam = 1}) => {
        const row = await api.get(`/gallery/list?page=${pageParam}`);

        return {
            row:row.data,
            current_page:pageParam,
            isLast: row.data.last
        }
    }

    const {
        data: getGallery,
        fetchNextPage: getNextPage,
        isSuccess: getGalleryIsSuccess,
        hasNextPage: getNextPageIsPossible
    } = useInfiniteQuery(["page_gallery_list"], getPageGallery, {
        getNextPageParam: (lastPage, pages) => {
            console.log(lastPage);
            // lastPage와 pages는 콜백함수에서 리턴한 값을 의미한다!!
            // lastPage: 직전에 반환된 리턴값, pages: 여태 받아온 전체 페이지
            if (!lastPage.isLast) return lastPage.current_page + 1;
            // 마지막 페이지면 undefined가 리턴되어서 hasNextPage는 false가 됨!
            return undefined;
        },
    });
    return { getGallery, getNextPage, getGalleryIsSuccess, getNextPageIsPossible };
}

export {getGalleryList,getGalleryView,deleteGalleryRemove}