"use strict";
exports.id = 728;
exports.ids = [728];
exports.modules = {

/***/ 6815:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(580);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);




const HeadLayout = ({ title  })=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                    children: title
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("script", {
                    dangerouslySetInnerHTML: {
                        __html: `history.scrollRestoration = "manual"`
                    }
                })
            ]
        })
    });
};
HeadLayout.propTypes = {
    title: (prop_types__WEBPACK_IMPORTED_MODULE_3___default().string.isRequired)
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HeadLayout);


/***/ }),

/***/ 6805:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((initValue = null)=>{
    const { 0: value , 1: setter  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(initValue);
    const handler = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)((e)=>{
        console.log(value);
        setter(e.target.value);
    }, []);
    return [
        value,
        handler,
        setter
    ];
});


/***/ }),

/***/ 6703:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Cq": () => (/* binding */ successAlert),
/* harmony export */   "Ge": () => (/* binding */ successAlertUrl),
/* harmony export */   "Q0": () => (/* binding */ errorAlertUrl),
/* harmony export */   "_1": () => (/* binding */ confirmAlert),
/* harmony export */   "sD": () => (/* binding */ errorAlert)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(271);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);



const errorAlert = (ref, title, text)=>{
    return sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        icon: "error",
        title: title,
        text: text,
        showCloseButton: true,
        focusConfirm: true,
        didClose: ()=>{
            if (ref != null) {
                ref.current.focus();
            }
        }
    });
};
const errorAlertUrl = (ref, title, text, url)=>{
    return sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        icon: "error",
        title: title,
        text: text,
        showCloseButton: true,
        focusConfirm: true,
        didClose: ()=>{
            window.location.replace(url);
        }
    });
};
const successAlertUrl = (title, text, url)=>{
    return sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        icon: "success",
        title: title,
        text: text,
        showCloseButton: true,
        focusConfirm: true,
        didClose: ()=>{
            if (url !== "") {
                next_router__WEBPACK_IMPORTED_MODULE_2___default().push(url);
            }
        }
    });
};
const successAlert = (title, text)=>{
    return sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        icon: "success",
        title: title,
        text: text,
        showCloseButton: true,
        focusConfirm: true
    });
};
const confirmAlert = (title, text)=>{
    return sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        title: title,
        text: text,
        icon: "question",
        confirmButtonText: "확인",
        showCloseButton: true,
        showDenyButton: true
    });
};


/***/ })

};
;