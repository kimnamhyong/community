"use strict";
exports.id = 990;
exports.ids = [990];
exports.modules = {

/***/ 2990:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(580);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8130);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _mui_material_Button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3819);
/* harmony import */ var _mui_material_Button__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Button__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _mui_lab__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6072);
/* harmony import */ var _mui_lab__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_mui_lab__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _hooks_useInput__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6805);
/* harmony import */ var _mui_icons_material_MapsUgc__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9020);
/* harmony import */ var _mui_icons_material_MapsUgc__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_MapsUgc__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _util_sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6703);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1175);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _hooks_api__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6040);
/* harmony import */ var _hooks_api_commentApi__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(7093);
/* harmony import */ var react_moment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(661);
/* harmony import */ var react_moment__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_moment__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var react_nl2br__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3572);
/* harmony import */ var react_nl2br__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(react_nl2br__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var react_js_pagination__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(5636);
/* harmony import */ var react_js_pagination__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_js_pagination__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _hooks_api_boardApi__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(3919);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_hooks_api__WEBPACK_IMPORTED_MODULE_10__, _hooks_api_commentApi__WEBPACK_IMPORTED_MODULE_11__, _hooks_api_boardApi__WEBPACK_IMPORTED_MODULE_15__]);
([_hooks_api__WEBPACK_IMPORTED_MODULE_10__, _hooks_api_commentApi__WEBPACK_IMPORTED_MODULE_11__, _hooks_api_boardApi__WEBPACK_IMPORTED_MODULE_15__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);
















const BoardComment = ({ user , board , id  })=>{
    const { 0: loading , 1: setLoading  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const commentRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
    const [content, onChangeContent, setContent] = (0,_hooks_useInput__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z)("");
    const { 0: comment_id , 1: setCommentId  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(0);
    const { 0: mode , 1: setMode  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
    const { 0: page , 1: setPage  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);
    const commentRemove = (0,react_query__WEBPACK_IMPORTED_MODULE_9__.useMutation)("commentRemove", _hooks_api_commentApi__WEBPACK_IMPORTED_MODULE_11__/* .deleteCommentRemove */ .o, {
        onSuccess: (data, variables, context)=>{
            row.refetch();
        },
        onError: (error, variables, context)=>{}
    });
    //코멘트 목록 가져오기
    const row = (0,react_query__WEBPACK_IMPORTED_MODULE_9__.useQuery)([
        "commentList",
        {
            page: page,
            type: board,
            board_id: id
        }
    ], _hooks_api_commentApi__WEBPACK_IMPORTED_MODULE_11__/* .getCommentList */ .R, {
        enabled: true
    });
    //코멘트 서버 전송하기
    const commentWrite = (0,react_query__WEBPACK_IMPORTED_MODULE_9__.useMutation)("commentUpdate", async (params)=>{
        await _hooks_api__WEBPACK_IMPORTED_MODULE_10__/* ["default"].post */ .Z.post(`/comment/${board}/update`, params).then((result)=>{
            setContent("");
            setLoading(false);
            row.refetch();
        }).catch((error)=>{
            console.log(error);
            setLoading(false);
        });
    });
    //댓글 등록을 눌렀을 때
    const onCommentWriteClick = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)(()=>{
        if (content.length < 2) {
            (0,_util_sweetalert2__WEBPACK_IMPORTED_MODULE_8__/* .errorAlert */ .sD)(commentRef, "댓글 내용 오류", "댓글을 입력하세요");
            return;
        }
        const comment = {
            user_id: user.user_id,
            user_name: user.user_name,
            content: content,
            board_id: id,
            page: page,
            mode: mode,
            comment_id: comment_id
        };
        commentWrite.mutate(comment);
        setLoading(true);
        setMode("");
    }, [
        user,
        page,
        content,
        comment_id,
        mode
    ]);
    //수정버튼을 눌렀을 때
    const onCommentEditClick = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((data)=>{
        setMode("update");
        setCommentId(data.id);
        setContent(data.content);
        commentRef.current.focus();
    }, [
        commentRef
    ]);
    //삭제 버튼을 눌렀을 때
    const onCommentRemoveClick = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((data)=>{
        if (mode === "update") {
            (0,_util_sweetalert2__WEBPACK_IMPORTED_MODULE_8__/* .errorAlert */ .sD)(null, "댓글 수정중", "댓글 수정중에는 삭제를 할 수 없습니다.");
            return;
        }
        (0,_util_sweetalert2__WEBPACK_IMPORTED_MODULE_8__/* .confirmAlert */ ._1)("댓글 삭제", "삭제하시겠습니까?").then((result)=>{
            if (result.isConfirmed) {
                commentRemove.mutate({
                    comment_user_id: data.user_id,
                    user_id: user.user_id,
                    comment_id: data.id,
                    type: board
                });
            }
        });
    }, [
        board,
        user,
        mode
    ]);
    //페이지 눌렀을 때
    const handlePageChange = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((page)=>{
        setPage(page);
        row.refetch();
    }, [
        page
    ]);
    //이거를 항상 넣어야 함 안 넣으면 새로 고침시 오류가 발생함
    if (row.isLoading) {
        return "loading";
    }
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__.Box, {
                component: "form",
                noValidate: true,
                autoComplete: "off",
                children: [
                    user ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("textarea", {
                                id: "content",
                                className: "comment-form",
                                placeholder: "내용을 입력하세요",
                                ref: commentRef,
                                value: content,
                                onChange: onChangeContent
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_mui_lab__WEBPACK_IMPORTED_MODULE_5__.LoadingButton, {
                                className: "login-btn",
                                variant: "outlined",
                                loading: loading,
                                loadingPosition: "start",
                                onClick: onCommentWriteClick,
                                startIcon: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_icons_material_MapsUgc__WEBPACK_IMPORTED_MODULE_7___default()), {}),
                                children: [
                                    "코멘트 ",
                                    mode === "update" ? "수정" : "등록"
                                ]
                            })
                        ]
                    }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        children: "댓글은 로그인 후에 쓸 수 있습니다."
                    }),
                    row.data?.row.map((data, key)=>{
                        return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "comment-list",
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "comment-title",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "comment-user-profile",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                src: `${_hooks_api__WEBPACK_IMPORTED_MODULE_10__/* .baseUrl */ .F}/uploads/${data.profile}`
                                            })
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "comment-user",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "comment-user-name",
                                                    children: data.user_name
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_moment__WEBPACK_IMPORTED_MODULE_12___default()), {
                                                    format: "YY/MM/DD",
                                                    className: "comment-creatAt",
                                                    children: data.createdAt
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "comment-content",
                                    children: react_nl2br__WEBPACK_IMPORTED_MODULE_13___default()(data.content)
                                }),
                                data.user_id === user?.user_id ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "comment-btn",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Button__WEBPACK_IMPORTED_MODULE_4___default()), {
                                            variant: "contained",
                                            color: "secondary",
                                            onClick: ()=>{
                                                onCommentEditClick(data);
                                            },
                                            children: "수정"
                                        }),
                                        "\xa0",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Button__WEBPACK_IMPORTED_MODULE_4___default()), {
                                            variant: "contained",
                                            color: "error",
                                            onClick: ()=>{
                                                onCommentRemoveClick(data);
                                            },
                                            children: "삭제"
                                        })
                                    ]
                                }) : null
                            ]
                        });
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "paging",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_js_pagination__WEBPACK_IMPORTED_MODULE_14___default()), {
                            activePage: page,
                            itemsCountPerPage: row.data?.pages.limit,
                            totalItemsCount: row.data?.pages.totalCount,
                            pageRangeDisplayed: row.data?.pages.pageSize,
                            prevPageText: "‹",
                            nextPageText: "›",
                            onChange: handlePageChange
                        })
                    })
                ]
            })
        })
    });
};
BoardComment.propTypes = {
    user: (prop_types__WEBPACK_IMPORTED_MODULE_2___default().object.isRequired),
    board: (prop_types__WEBPACK_IMPORTED_MODULE_2___default().string.isRequired)
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BoardComment);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7093:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "R": () => (/* binding */ getCommentList),
/* harmony export */   "o": () => (/* binding */ deleteCommentRemove)
/* harmony export */ });
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6040);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_api__WEBPACK_IMPORTED_MODULE_0__]);
_api__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];

//댓글 목록
const getCommentList = async ({ queryKey  })=>{
    const [_key, { page , type , board_id  }] = queryKey;
    const { data  } = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].get */ .Z.get(`/comment/${type}/list?board_id=${board_id}&page=${page}`);
    return data;
};
//게시판 삭제하기
const deleteCommentRemove = async (params)=>{
    console.log(params);
    if (params.comment_user_id === params.user_id) {
        const { data  } = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"]["delete"] */ .Z["delete"](`/comment/${params.type}/remove/${params.comment_id}`);
        console.log(data);
        return data;
    } else {
        return null;
    }
};


__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;