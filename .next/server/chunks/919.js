"use strict";
exports.id = 919;
exports.ids = [919];
exports.modules = {

/***/ 3919:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "bO": () => (/* binding */ boardInfiniteScrollQuery),
/* harmony export */   "lN": () => (/* binding */ getBoardView),
/* harmony export */   "lS": () => (/* binding */ deleteBoardRemove)
/* harmony export */ });
/* unused harmony export getBoardList */
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6040);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1175);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_1__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_api__WEBPACK_IMPORTED_MODULE_0__]);
_api__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];

 //무한스크롤 할 때 필요함
//게시판 목록
const getBoardList = async ({ queryKey  })=>{
    const [_key, { page  }] = queryKey;
    const { data  } = await api.get(`/board/list?page=${page}`);
    console.log(data);
    return data;
};
//게시판 상세보기
const getBoardView = async ({ queryKey  })=>{
    const [_key, { id  }] = queryKey;
    const { data  } = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].get */ .Z.get(`/board/view?id=${id}`);
    console.log(data);
    return data;
};
//게시판 삭제하기
const deleteBoardRemove = async (params)=>{
    console.log(params);
    if (params.board_user_id === params.user_id) {
        const { data  } = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"]["delete"] */ .Z["delete"](`/board/remove/${params.id}`);
        console.log(data);
        return data;
    } else {
        return null;
    }
};
//무한 스크롤 게시판 목록
const boardInfiniteScrollQuery = ()=>{
    const getPageBoard = async ({ pageParam =1  })=>{
        const row = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].get */ .Z.get(`/board/list?page=${pageParam}`);
        return {
            row: row.data,
            current_page: pageParam,
            isLast: row.data.last
        };
    };
    const { data: getBoard , fetchNextPage: getNextPage , isSuccess: getBoardIsSuccess , hasNextPage: getNextPageIsPossible  } = (0,react_query__WEBPACK_IMPORTED_MODULE_1__.useInfiniteQuery)([
        "page_board_list"
    ], getPageBoard, {
        getNextPageParam: (lastPage, pages)=>{
            // lastPage와 pages는 콜백함수에서 리턴한 값을 의미한다!!
            // lastPage: 직전에 반환된 리턴값, pages: 여태 받아온 전체 페이지
            if (!lastPage.isLast) return lastPage.current_page + 1;
            // 마지막 페이지면 undefined가 리턴되어서 hasNextPage는 false가 됨!
            return undefined;
        }
    });
    return {
        getBoard,
        getNextPage,
        getBoardIsSuccess,
        getNextPageIsPossible
    };
};


__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;