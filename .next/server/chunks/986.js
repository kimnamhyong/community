"use strict";
exports.id = 986;
exports.ids = [986];
exports.modules = {

/***/ 3986:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JZ": () => (/* binding */ galleryInfiniteScrollQuery),
/* harmony export */   "Vc": () => (/* binding */ getGalleryView),
/* harmony export */   "ld": () => (/* binding */ deleteGalleryRemove)
/* harmony export */ });
/* unused harmony export getGalleryList */
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6040);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1175);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_1__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_api__WEBPACK_IMPORTED_MODULE_0__]);
_api__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];

 //무한스크롤 할 때 필요함
//갤러리 목록
const getGalleryList = async ({ queryKey  })=>{
    const [_key, { page  }] = queryKey;
    const { data  } = await api.get(`/gallery/list?page=${page}`);
    console.log(data);
    return data;
};
//갤러리 상세보기
const getGalleryView = async ({ queryKey  })=>{
    const [_key, { id  }] = queryKey;
    const { data  } = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].get */ .Z.get(`/gallery/view?id=${id}`);
    console.log(data);
    return data;
};
//갤러리 삭제하기
const deleteGalleryRemove = async (params)=>{
    console.log(params);
    if (params.board_user_id === params.user_id) {
        const { data  } = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"]["delete"] */ .Z["delete"](`/gallery/remove/${params.id}`);
        console.log(data);
        return data;
    } else {
        console.log("111");
        return null;
    }
};
//무한 스크롤 갤러리 목록
const galleryInfiniteScrollQuery = ()=>{
    const getPageGallery = async ({ pageParam =1  })=>{
        const row = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].get */ .Z.get(`/gallery/list?page=${pageParam}`);
        return {
            row: row.data,
            current_page: pageParam,
            isLast: row.data.last
        };
    };
    const { data: getGallery , fetchNextPage: getNextPage , isSuccess: getGalleryIsSuccess , hasNextPage: getNextPageIsPossible  } = (0,react_query__WEBPACK_IMPORTED_MODULE_1__.useInfiniteQuery)([
        "page_gallery_list"
    ], getPageGallery, {
        getNextPageParam: (lastPage, pages)=>{
            console.log(lastPage);
            // lastPage와 pages는 콜백함수에서 리턴한 값을 의미한다!!
            // lastPage: 직전에 반환된 리턴값, pages: 여태 받아온 전체 페이지
            if (!lastPage.isLast) return lastPage.current_page + 1;
            // 마지막 페이지면 undefined가 리턴되어서 hasNextPage는 false가 됨!
            return undefined;
        }
    });
    return {
        getGallery,
        getNextPage,
        getGalleryIsSuccess,
        getNextPageIsPossible
    };
};


__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;